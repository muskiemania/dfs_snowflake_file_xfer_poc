from lambdas import new_file_delivered_s3
import urllib
import re
import boto3
import os

def test_get_bucket():

    bucket_name = 'my bucket'
    event = {
        'Records': [
            {'s3':
             {
                 'bucket': {
                     'name': bucket_name
                 }
             }}
        ]
    }

    assert new_file_delivered_s3.get_bucket(event) == bucket_name


def get_key():

    key = 'abc/def/ghi/jkl.mno'
    event = {
        'Records': [
            {
                's3': {
                    'object': {
                        'key': urllib.parse.quote_plus(key.encode('utf8'))
                    }
                }
            }
        ]
    }

    assert new_file_delivered_s3.get_key(event) == key


def test_get_tags(monkeypatch):
    tags = [1, 2, 3, 4]

    def mock_get_object_tags(**kwargs):
        return {'TagSet': tags}

    client = boto3.client('s3')
    monkeypatch.setattr(client, 'get_object_tagging', mock_get_object_tags)
    assert new_file_delivered_s3.get_tags(client, 'Bucket', 'Key') == tags


def test_get_metadata(monkeypatch):
    meta = {
        'key1': 'value1',
        'key2': 'value2'
    }

    def mock_get_head_object(**kwargs):
        return {'Metadata': meta}

    client = boto3.client('s3')
    monkeypatch.setattr(client, 'head_object', mock_get_head_object)
    assert new_file_delivered_s3.get_metadata(client, 'Bucket', 'Key') == meta


def test_get_item_keys():

    key_parts = ['subfolder1', 'subfolder2',
                 'subfolder3', 'partition', 'file.avro']
    key = '/'.join(key_parts)

    (key_minus, filename) = new_file_delivered_s3.get_item_keys(key)

    assert key_minus == 'subfolder1/subfolder2/subfolder3/partition'
    assert filename == 'file.avro'


def test_get_timestamp():

    timestamp = new_file_delivered_s3.get_timestamp()

    assert re.match(
        '\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}[+-]\\d{2}:\\d{2}', timestamp)


def test_get_table_name(monkeypatch):

    table_name = 'table name'
    monkeypatch.setattr(os, 'environ', {'dynamo_table': table_name})

    assert new_file_delivered_s3.get_table_name() == table_name
