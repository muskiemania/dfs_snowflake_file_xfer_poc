import boto3
import urllib
import os
import pytz
import datetime


def handler(event, context):

    client = boto3.client('s3')
    bucket = get_bucket(event)
    key = get_key(event)

    tags = get_tags(client, bucket, key)
    meta = get_metadata(client, bucket, key)

    (key_minus_filename, filename) = get_item_keys(key)
    timestamp = get_timestamp()

    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(get_table_name())

    table.put_item(Item={'partition_key': key_minus_filename,
                         'sort_key': filename,
                         'bucket_name': bucket,
                         'key': key,
                         'metadata': meta,
                         'tags': tags,
                         'created': timestamp})


def get_bucket(event):
    return event['Records'][0]['s3']['bucket']['name']


def get_key(event):
    return urllib.parse.unquote_plus(
        event['Records'][0]['s3']['object']['key']).decode('utf8')


def get_tags(client, bucket, key):
    obj = client.get_object_tagging(Bucket=bucket, Key=key)
    return 'TagSet' in obj.keys() and obj['TagSet'] or []


def get_metadata(client, bucket, key):
    obj = client.head_object(Bucket=bucket, Key=key)
    return 'Metadata' in obj.keys() and obj['Metadata'] or {}


def get_item_keys(key):
    arr = key.split('/')
    return ('/'.join(arr[:-1]), arr[-1])


def get_timestamp():
    utc = pytz.utc.localize(datetime.datetime.utcnow())
    ct = utc.astimezone(pytz.timezone('America/Chicago'))
    return ct.replace(microsecond=0).isoformat()


def get_table_name():
    return os.environ['dynamo_table'] or 'jay_test_tbl'
